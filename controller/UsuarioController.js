/**
 * Created by Jose on 21/05/2016.
 */
module.exports =  function (app) {
    return {
        add: function (req, res) {
            var Usuario = app.get('usuario');
            Usuario.create({
                nombre: req.body.nombre,
                correo: req.body.correo,
                telefono: req.body.telefono,
                nick: req.body.nick,
                contraseña: req.body.contraseña,
            }).then(function (usuario) {
                res.json(usuario);
            });
        },
        list: function (req, res) {
            var Usuario = app.get('usuario');
            Usuario.findAll().then(function (usuario) {
                res.json(usuario);
            });
        },
        edit: function (req, res) {
            var Usuario = app.get('usuario');
            Usuario.find(req.body.id_usuario).then(function (usuario) {
                if(usuario){
                    usuario.updateAttributes({
                        nombre: req.body.nombre,
                        correo: req.body.correo,
                        telefono: req.body.telefono,
                        nick: req.body.nick,
                        contraseña: req.body.contraseña
                    });
                } else {
                    res.status(404).send({message: "Usuario no encontrado"});
                }
            });
        },
        delete: function (req, res) {
            var Usuario = app.get('usuario');
            Usuario.destroy({
                where: {
                    id_usuario: req.body.id_usuario
                }
            }).then(function (usuario) {
                res.json(usuario);
            });
        }
    }
}
