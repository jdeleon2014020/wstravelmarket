/**
 * Created by Jose on 27/05/2016.
 */
module.exports = function (app) {
    return{
        add: function (req, res) {
            var Hotel = app.get('hotel');
            Hotel.create({
                nombre: req.body.nombre,
                descripcion: req.body.descripcionn,
                costo: req.body.costo,
                id_lugarturistico: req.body.id_lugarturistico
            }).then(function (hotel) {
                res.json(hotel);
            })
        },
        list: function (req, res) {
            var Hotel = app.get('hotel');
            Hotel.findAll().then(function (hotel) {
                res.json(hotel);
            }); 
        },
        edit: function (req, res) {
            var Hotel = app.get('hotel');
            Hotel.find(req.body.id_hotel).then(function (hotel) {
                if(hotel){
                    hotel.updateAttributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcionn,
                        costo: req.body.costo,
                        id_lugarturistico: req.body.id_lugarturistico
                    })
                }else{
                    req.status(404).send({message: "Hotel no encontrado"})
                }
            });
        },
        delete: function (req, res) {
            var Hotel = app.get('hotel');
            Hotel.destroy({
                where: {
                    id_hotel: req.body.id_hotel
                }
            }).then(function (hotel) {
                    res.json(hotel);
            });
        }
    }
}
