/**
 * Created by Jose on 27/05/2016.
 */
module.exports = function (app) {
    return{
        add: function (req, res) {
            var LugarTuristico = app.get('lugarturistico');
            LugarTuristico.create({
                nombre: req.body.nombre,
                distancia: req.body.distancia,
                descripcion: req.body.descripcion,
                id_departamento: req.body.id_departamento
            }).then(function (lugarturistico) {
                res.json(lugarturistico);
            });
        },
        list: function (req, res) {
            var LugarTuristico = app.get('lugarturistico');
            LugarTuristico.findAll().then(function (lugarturistico) {
                res.json(lugarturistico);
            });
        },
        edit: function (req, res) {
            var LugarTuristico = app.get('lugarturistico');
            LugarTuristico.find(req.body.id_lugarturistico).then(function (lugarturistico) {
                if(lugarturistico){
                    lugarturistico.updateAttributes({
                        nombre: req.body.nombre,
                        distancia: req.body.distancia,
                        descripcion: req.body.descripcion,
                        id_departamento: req.body.id_departamento
                    })
                }else{
                    req.status(404).send({message: "Lugar turistico no encontrado"})
                }
            });
        },
        delete: function (req, res) {
            var LugarTuristico = app.get('lugarturistico');
            LugarTuristico.destroy({
                where: {
                    id_lugarturistico: req.body.id_lugarturistico
                }
            }).then(function (lugarturistico) {
                res.json(lugarturistico);
            });
        }
    }
}
