/**
 * Created by Jose on 26/05/2016.
 */
module.exports = function (app){
    return{
        add: function (req, res) {
            var Rol = app.get('rol');
            Rol.create({
                nombre: req.body.nombre,
                descripcion: req.body.descripcion
            }).then(function (rol) {
                res.json(rol);
            });
        },
        list: function (req, res) {
            var Rol = app.get('rol');
            Rol.findAll().then(function (rol) {
                res.json(rol);
            });
        },
        edit: function (req, res) {
            var Rol = app.get('rol');
            Rol.find(req.body.id_rol).then(function (rol) {
                if (rol) {
                    rol.updateAttributes({
                        nombre: req.body.nombre,
                        descripcion: req.body.descripcion
                    });
                    res.json({"mensaje":"El rol "+rol.nombre+" fue modificado de manera correcta."})
                } else {
                    req.status(404).send({message: "Rol no encontrado"});
                }
            })
        },
        delete: function (req, res) {
            var Rol = app.get('rol');
            Rol.destroy({
            where: {
                id_rol: req.body.id_rol
            }
            }).then(function (rol) {
                    res.json(rol);
            });
        }
    }
}
