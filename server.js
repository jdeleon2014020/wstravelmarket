(function () {
    var express = require('express');
    var bodyparser = require('body-parser');
    var mysql = require('mysql');
    var Sequelize = require('sequelize');
    var puerto = 3300;
    var app = express();

    var sequelize = new Sequelize('db_TravelMarket', 'root','',{
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 20,
            min: 0
        }
    });

    /*DEFINICION DE MODELOS*/
    var Rol= sequelize.define('rol', {
        id_rol: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        descripcion: {type: Sequelize.STRING, allowNull: false}
    });

    var Usuario = sequelize.define('usuario', {
        id_usuario: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        correo: {type: Sequelize.STRING, allowNull: false},
        telefono: {type: Sequelize.INTEGER, allowNull: false},
        nick: {type: Sequelize.STRING, allowNull: false},
        contraseña: {type: Sequelize.STRING, allowNull: false},
        id_rol: {type: Sequelize.INTEGER, foreignKey:true}
    });

    var Departamento = sequelize.define('departamento',{
        id_departamento: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        descripcion: {type: Sequelize.STRING, allowNull: false}
    });
    var LugarTuristico = sequelize.define('lugarturistico',{
        id_lugarturistico: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        distancia: {type: Sequelize.INTEGER, allowNull: false},
        descripcion: {type: Sequelize.STRING, allowNull: false},
        id_departamento: {type: Sequelize.INTEGER, foreignKey:true}
    });

    var Hotel = sequelize.define('hotel', {
        id_hotel: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        descripcion: {type: Sequelize.STRING, allowNull: false},
        costo: {type: Sequelize.INTEGER, allowNull: false},
        id_lugarturistico: {type: Sequelize.INTEGER, foreignKey: true}
    });

    var Tour = sequelize.define('tour',{
        id_tour: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: Sequelize.STRING, allowNull: false},
        descripcion: {type: Sequelize.STRING, allowNull: false},
        id_hotel: {type: Sequelize.INTEGER, foreignKey: true},
        id_lugarturistico: {type: Sequelize.INTEGER, foreignKey: true},
        id_usuario: {type: Sequelize.INTEGER, foreignKey: true}
    });
    /*DEFINICION DE MODELOS*/

    /*LLAVES FORANEAS*/
    Rol.hasMany(Usuario, {foreignKey: 'íd_rol', constraints: true});
    Usuario.belongsTo(Rol, {foreignKey: 'id_rol', constraints: true});

    Tour.hasMany(Usuario, {foreignKey: 'id_usuario',constraints: true});
    Usuario.belongsTo(Tour, {foreignKey: 'id_usuario', constraints: true});

    Departamento.hasMany(LugarTuristico, {foreignKey: 'id_departamento', constraints: true});
    LugarTuristico.belongsTo(Departamento, {foreignKey: 'id_departamento', constraints: true});

    LugarTuristico.hasMany(Hotel,{foreignKey: 'id_lugarturistico', constraints: true});
    Hotel.belongsTo(LugarTuristico, {foreignKey: 'id_lugarturistico', constraints: true});

    Hotel.hasMany(Tour, {foreignKey: 'id_hotel', constraints: true});
    Tour.belongsTo(Hotel, {foreignKey: 'id_hotel', constraints: true});

    LugarTuristico.hasMany(Tour, {foreignKey: 'id_lugarturistico', constraints: true});
    Tour.belongsTo(LugarTuristico, {foreignKey: 'id_lugarturistico', constraints: true});
    /*LLAVES FORANEAS*/

    sequelize.sync({force : true});
    var conf=require('./config');

    app.use(bodyparser.urlencoded({
        extended: true
    }));
    app.use(bodyparser.json());
    app.use('/api/v1', require('./routes')(app));
    app.set('rol', Rol);
    app.set('usuario', Usuario);
    app.set('departamento', Departamento);
    app.set('lugarturistico', LugarTuristico);
    app.set('hotel', Hotel);
    app.set('tour', Tour);
    app.listen(puerto,function(){
        console.log("Servidor iniciado en el puerto: " +puerto);
        console.log("Debug del servidor");
    });
})();