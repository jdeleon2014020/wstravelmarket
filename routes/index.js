/**
 * Created by Jose on 21/05/2016.
 */
var ruta = require('express').Router();
module.exports = (function (app) {
    var departamento = require('../controller/DepartamentoController')(app);
    var rol = require('../controller/RolController')(app);
    var usuario = require('../controller/UsuarioController')(app);
    var hotel = require('../controller/HotelController')(app);
    var lugarTuristico = require ('../controller/LugarTuristicoController')(app);

    /*Rutas de Departamento*/
    ruta.get('/departamento', departamento.list);
    ruta.post('/departamento', departamento.add);
    ruta.put('/departamento', departamento.edit);
    ruta.delete('/departamento', departamento.delete);
    ruta.get('/departamento/:id', departamento.departamentoconlugares);
    /*Rutas de Departamento*/

    /*Rutas de Rol*/
    ruta.get('/rol', rol.list);
    ruta.post('/rol', rol.add);
    ruta.put('/rol/:id', rol.edit);
    ruta.delete('/rol', rol.delete);
    /*Rutas de Rol*/

    /*Rutas de Usuario*/
    ruta.get('/usuario', usuario.list);
    ruta.post('/usuario', usuario.add);
    ruta.put('/usuario/:id', usuario.edit);
    ruta.delete('/usuario', usuario.delete);
    /*Rutas de Usuario*/

    /*Rutas de hotel*/
    ruta.get('/hotel', hotel.list);
    ruta.post('/hotel', hotel.add);
    ruta.put('/hotel/:id', hotel.edit);
    ruta.delete('/hotel', hotel.delete);
    /*Rutas de hotel*/

    /*Rutas lugarTuristico*/
    ruta.get('/lugarTuristico', lugarTuristico.list);
    ruta.post('/lugarTuristico', lugarTuristico.add);
    ruta.put('/lugarTuristico/:id', lugarTuristico.edit);
    ruta.delete('/lugarTuristico', lugarTuristico.delete);
    /*Rutas lugarTuristico*/

    return ruta;
});